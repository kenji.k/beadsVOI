function betchoice_modelcomp
%% Model fit and comparison on betting behavior in bet-only trials, i.e., whether to bet on the high-reward or low-reward jar, as a function of beads difference.
%% Use gaussian process logistic regression to estimate underlying utility function.
%% Model comparison uses cross validation, leave-one-participant-out (LOPO) and leave-one-trial-out (LOTO). Visualization of model fit at the end.

%% this uses GPML toolbox (Rasmussen & Nickisch, 2010; Rasmussen & Williams, 2006), available at www.gaussianprocess.org/gpml/code/matlab/doc/ and github (github.com/alshedivat/gpml).

%% In the paper, we compared the following models:
%%%  Model 1: sensitivity to reward manipulation in scale block but not in baseline block
%%%  Model 2: sensitivity to reward manipulation in both baseline and scale blocks
%%%  Model 3: no sensitivity to reward manipulation in baseline or scale block, but different value function between blocks
%%%  Model 4: no sensitivity to reward manipulation, same value function for both blocks
%%%  Model 6: same as Model 3, except that the value function is different between the first and second scanning runs
%%% (Model 5 was not used for betting analysis, see infoseeking_modelcomp.m for details)

%% written by Kenji Kobayashi

% GPML toolbox setup. Note that you first need to have GPML toolbox.
gpmldir = 'gpml-matlab-master/';
run([gpmldir, 'startup.m'])
meanfunc = @meanConst;
covfunc = @covSEiso;
likfunc = @likLogistic;
inffunc = @infVB;
numeval = -200;

%% load and concatenate all participants' data into vectors
datadir = 'behavdata/';
subnames = dir([datadir, '*.mat']);
conc_blockindices = [];
conc_sessionindices = [];
conc_beadsdif = [];
conc_rew_corr_H = [];
conc_betchoice = [];
subindices = [];

for sub = 1:length(subnames)
	load([datadir, subnames(sub).name], 'gameindices', 'sessionindices', 'init_beads_H', 'init_beads_L', 'rew_corr_H', 'info', 'choice');

	% extract bet-only trials only
	bet_trial = info(:, 1) == 0;								% if zero, participants could not draw additional beads (i.e., bet-only trial)
	gameindices = gameindices(bet_trial);						% 1 (baseline block) or 2 (scale block)
	sessionindices = sessionindices(bet_trial);					% 1 or 2, scanning run within block
	rew_corr_H = rew_corr_H(bet_trial);						% 70 or 170 in baseline block, 70 or 7 in scale block
	choice = choice(bet_trial);								% 1: bet on the high-reward jar, 2: bet on the low-reward jar

	% the number of beads presented before a bet
	beadsdif = init_beads_H(bet_trial) - init_beads_L(bet_trial);	% from -10 to 10, 2 increment

	conc_blockindices = [conc_blockindices; gameindices];
	conc_sessionindices = [conc_sessionindices; sessionindices];
	conc_beadsdif = [conc_beadsdif; beadsdif];
	conc_rew_corr_H = [conc_rew_corr_H; rew_corr_H];
	conc_betchoice = [conc_betchoice; choice];

	subindices = [subindices; sub*ones(length(choice), 1)];	% participant idx for leave-one-participant-out CV, from 1 to 15
end

blockindices = conc_blockindices;
sessindices = conc_sessionindices;
rewindices = conc_rew_corr_H == 70;	%% whether the high reward is 70 or not (may be 170 or 7)
beadsdif = conc_beadsdif;
betchoice = -2 * conc_betchoice + 3;	%% 1: bet on the high-reward jar, -1: bet on the low-reward jar

%% model comparison 
% To evaluate Models 1 to 4, compute CV log likelihood for each trial with 1) block-agnostic, reward-agnostic function, 2) block-specific, reward-agnostic function, and 3) block-specific, reward-specific latent value function
% To evaluate Model 6, compute CV with 4) session-specific, reward-agnostic function
% For each trial, CV log likelihood for each model is computed for LOPO and LOTO

%% before CV, estimate hyperparameters using all data
hyp_noblock_noreward.mean = 0; hyp_noblock_noreward.cov = [0; 0];
hyp_noblock_noreward = minimize(hyp_noblock_noreward, @gp, numeval, inffunc, meanfunc, covfunc, likfunc, beadsdif, betchoice);

for blockidx = 1:2	% trials in which block to evaluate

	hyp_block_noreward{blockidx}.mean = 0; hyp_block_noreward{blockidx}.cov = [0; 0];
	hyp_block_noreward{blockidx} = minimize(hyp_block_noreward{blockidx}, @gp, numeval, inffunc, meanfunc, covfunc, likfunc, beadsdif(blockindices == blockidx), betchoice(blockindices == blockidx));

	for rewidx = 0:1	% trials in which reward condition to evaluate, 70 or not
		hyp_block_reward{blockidx, rewidx+1}.mean = 0; hyp_block_reward{blockidx, rewidx+1}.cov = [0; 0];
		hyp_block_reward{blockidx, rewidx+1} = minimize(hyp_block_reward{blockidx, rewidx+1}, @gp, numeval, inffunc, meanfunc, covfunc, likfunc, beadsdif(blockindices == blockidx & rewindices == rewidx), betchoice(blockindices == blockidx & rewindices == rewidx));
	end

	for sessidx = 1:2	% trials from which scanning run to evaluate in CV
		hyp_block_noreward_sess{blockidx, sessidx}.mean = 0; hyp_block_noreward_sess{blockidx, sessidx}.cov = [0; 0];
		hyp_block_noreward_sess{blockidx, sessidx} = minimize(hyp_block_noreward_sess{blockidx, sessidx}, @gp, numeval, inffunc, meanfunc, covfunc, likfunc, beadsdif(blockindices == blockidx & sessindices == sessidx), betchoice(blockindices == blockidx & sessindices == sessidx));
	end
end


%% LOPO
loglik_LOPO_noblock_noreward = zeros(length(betchoice), 1);
loglik_LOPO_block_noreward = zeros(length(betchoice), 1);
loglik_LOPO_block_reward = zeros(length(betchoice), 1);
loglik_LOPO_block_noreward_sess = zeros(length(betchoice), 1);

% leave out data from one participant, train the model using the rest, and test the model on the left-out participant data
for leave_subidx = 1:max(subindices)	%% which participant to leave out

	% training data
	train_blockindices = blockindices(subindices ~= leave_subidx);
	train_sessindices = sessindices(subindices ~= leave_subidx);
	train_rewindices = rewindices(subindices ~= leave_subidx);
	train_beadsdif = beadsdif(subindices ~= leave_subidx);
	train_betchoice = betchoice(subindices ~= leave_subidx);

	% testing data
	test_blockindices = blockindices(subindices == leave_subidx);
	test_sessindices = sessindices(subindices == leave_subidx);
	test_rewindices = rewindices(subindices == leave_subidx);
	test_beadsdif = beadsdif(subindices == leave_subidx);
	test_betchoice = betchoice(subindices == leave_subidx);

	% block-agnostic, reward-agnostic model
	hyp_noblock_noreward_CV = minimize(hyp_noblock_noreward, @gp, numeval, inffunc, meanfunc, covfunc, likfunc, train_beadsdif, train_betchoice);
	[~, ~, ~, ~, loglik_LOPO_noblock_noreward(subindices == leave_subidx)] = gp(hyp_noblock_noreward_CV, inffunc, meanfunc, covfunc, likfunc, train_beadsdif, train_betchoice, test_beadsdif, test_betchoice);

	for blockidx = 1:2	% trials in which block to evaluate

		% block-specific, reward-agnostic model
		hyp_block_noreward_CV = minimize(hyp_block_noreward{blockidx}, @gp, numeval, inffunc, meanfunc, covfunc, likfunc, train_beadsdif(train_blockindices == blockidx), train_betchoice(train_blockindices == blockidx));
		[~, ~, ~, ~, loglik_LOPO_block_noreward(subindices == leave_subidx & blockindices == blockidx)] = gp(hyp_block_noreward_CV, inffunc, meanfunc, covfunc, likfunc, train_beadsdif(train_blockindices == blockidx), train_betchoice(train_blockindices == blockidx), test_beadsdif(test_blockindices == blockidx), test_betchoice(test_blockindices == blockidx));

		for rewidx = 0:1	% trials in which reward condition to evaluate, 70 or not
			% block-specific, reward-specific model
			hyp_block_reward_CV = minimize(hyp_block_reward{blockidx, rewidx+1}, @gp, numeval, inffunc, meanfunc, covfunc, likfunc, train_beadsdif(train_blockindices == blockidx & train_rewindices == rewidx), train_betchoice(train_blockindices == blockidx & train_rewindices == rewidx));
			[~, ~, ~, ~, loglik_LOPO_block_reward(subindices == leave_subidx & blockindices == blockidx & rewindices == rewidx)] = gp(hyp_block_reward_CV, inffunc, meanfunc, covfunc, likfunc, train_beadsdif(train_blockindices == blockidx & train_rewindices == rewidx), train_betchoice(train_blockindices == blockidx & train_rewindices == rewidx), test_beadsdif(test_blockindices == blockidx & test_rewindices == rewidx), test_betchoice(test_blockindices == blockidx & test_rewindices == rewidx));
		end

		for sessidx = 1:2	% trials from which scanning run to evaluate in CV
			% block-specific, reward-agnostic, session-wise model
			hyp_block_noreward_sess_CV = minimize(hyp_block_noreward_sess{blockidx, sessidx}, @gp, numeval, inffunc, meanfunc, covfunc, likfunc, train_beadsdif(train_blockindices == blockidx & train_sessindices == sessidx), train_betchoice(train_blockindices == blockidx & train_sessindices == sessidx));
			[~, ~, ~, ~, loglik_LOPO_block_noreward_sess(subindices == leave_subidx & blockindices == blockidx & sessindices == sessidx)] = gp(hyp_block_noreward_sess_CV, inffunc, meanfunc, covfunc, likfunc, train_beadsdif(train_blockindices == blockidx & train_sessindices == sessidx), train_betchoice(train_blockindices == blockidx & train_sessindices == sessidx), test_beadsdif(test_blockindices == blockidx & test_sessindices == sessidx), test_betchoice(test_blockindices == blockidx & test_sessindices == sessidx));
		end
	end
end


%% LOTO
loglik_LOTO_noblock_noreward = zeros(length(betchoice), 1);
loglik_LOTO_block_noreward = zeros(length(betchoice), 1);
loglik_LOTO_block_reward = zeros(length(betchoice), 1);
loglik_LOTO_block_noreward_sess = zeros(length(betchoice), 1);

% leave out one trial, train the model using the rest, and test the model on the left-out trial data
% to speed up the computation, just leave out one (first) trial for each unique combination of experiment parameters that are relevant to the model being evaluated (bet choice, beads difference, block, reward condition, or session)
% for instance, for the model that assumes a block-specific but reward-agnostic value function, one trial from the baseline (or scale) block is left out, the model is trained on the remaining trials in the baseline (or scale) block, and the trained model is tested on the left-out trial

for leave_betchoice = [-1, 1]	% bet choice of the left-out trial
	for leave_beadsdif = -10:2:10	% beadsdif of the left-out trial

		% block-agnostic, reward-agnostic model
		leavetrialidx = find(beadsdif == leave_beadsdif & betchoice == leave_betchoice);
		if ~isempty(leavetrialidx)

			traintrialidx = [1:leavetrialidx(1)-1, leavetrialidx(1)+1:length(betchoice)];
			train_beadsdif = beadsdif(traintrialidx);
			train_betchoice = betchoice(traintrialidx);

			hyp_noblock_noreward_CV = minimize(hyp_noblock_noreward, @gp, numeval, inffunc, meanfunc, covfunc, likfunc, train_beadsdif, train_betchoice);
			[~, ~, ~, ~, loglik_LOTO_noblock_noreward(leavetrialidx)] = gp(hyp_noblock_noreward_CV, inffunc, meanfunc, covfunc, likfunc, train_beadsdif, train_betchoice, leave_beadsdif, leave_betchoice);
		end

		for leave_blockidx = 1:2	% block of the left-out trial

			% block-specific, reward-agnostic model
			leavetrialidx = find(blockindices == leave_blockidx & beadsdif == leave_beadsdif & betchoice == leave_betchoice);
			if ~isempty(leavetrialidx)

				traintrialidx = [1:leavetrialidx(1)-1, leavetrialidx(1)+1:length(betchoice)];
				train_blockindices = blockindices(traintrialidx);
				train_beadsdif = beadsdif(traintrialidx);
				train_betchoice = betchoice(traintrialidx);

				hyp_block_noreward_CV = minimize(hyp_block_noreward{leave_blockidx}, @gp, numeval, inffunc, meanfunc, covfunc, likfunc, train_beadsdif(train_blockindices == leave_blockidx), train_betchoice(train_blockindices == leave_blockidx));
				[~, ~, ~, ~, loglik_LOTO_block_noreward(leavetrialidx)] = gp(hyp_block_noreward_CV, inffunc, meanfunc, covfunc, likfunc, train_beadsdif(train_blockindices == leave_blockidx), train_betchoice(train_blockindices == leave_blockidx), leave_beadsdif, leave_betchoice);
			end

			for leave_rewidx = 0:1	% reward condition of the left-out trial, 70 or not

				% block-specific, reward-specific model
				leavetrialidx = find(blockindices == leave_blockidx & rewindices == leave_rewidx & beadsdif == leave_beadsdif & betchoice == leave_betchoice);
				if ~isempty(leavetrialidx)

					traintrialidx = [1:leavetrialidx(1)-1, leavetrialidx(1)+1:length(betchoice)];
					train_blockindices = blockindices(traintrialidx);
					train_rewindices = rewindices(traintrialidx);
					train_beadsdif = beadsdif(traintrialidx);
					train_betchoice = betchoice(traintrialidx);

					hyp_block_reward_CV = minimize(hyp_block_reward{leave_blockidx, leave_rewidx+1}, @gp, numeval, inffunc, meanfunc, covfunc, likfunc, train_beadsdif(train_blockindices == leave_blockidx & train_rewindices == leave_rewidx), train_betchoice(train_blockindices == leave_blockidx & train_rewindices == leave_rewidx));
					[~, ~, ~, ~, loglik_LOTO_block_reward(leavetrialidx)] = gp(hyp_block_reward_CV, inffunc, meanfunc, covfunc, likfunc, train_beadsdif(train_blockindices == leave_blockidx & train_rewindices == leave_rewidx), train_betchoice(train_blockindices == leave_blockidx & train_rewindices == leave_rewidx), leave_beadsdif, leave_betchoice);
				end
			end

			for leave_sessidx = 1:2	% scanning run of the left-out trial

				% block-specific, reward-specific, session-wise model
				leavetrialidx = find(blockindices == leave_blockidx & sessindices == leave_sessidx & beadsdif == leave_beadsdif & betchoice == leave_betchoice);
				if ~isempty(leavetrialidx)

					traintrialidx = [1:leavetrialidx(1)-1, leavetrialidx(1)+1:length(betchoice)];
					train_blockindices = blockindices(traintrialidx);
					train_sessindices = sessindices(traintrialidx);
					train_beadsdif = beadsdif(traintrialidx);
					train_betchoice = betchoice(traintrialidx);

					hyp_block_noreward_sess_CV = minimize(hyp_block_noreward_sess{leave_blockidx, leave_sessidx}, @gp, numeval, inffunc, meanfunc, covfunc, likfunc, train_beadsdif(train_blockindices == leave_blockidx & train_sessindices == leave_sessidx), train_betchoice(train_blockindices == leave_blockidx & train_sessindices == leave_sessidx));
					[~, ~, ~, ~, loglik_LOTO_block_noreward_sess(leavetrialidx)] = gp(hyp_block_noreward_sess_CV, inffunc, meanfunc, covfunc, likfunc, train_beadsdif(train_blockindices == leave_blockidx & train_sessindices == leave_sessidx), train_betchoice(train_blockindices == leave_blockidx & train_sessindices == leave_sessidx), leave_beadsdif, leave_betchoice);
				end
			end
		end
	end
end

save('betchoice_modelcomp.mat', 'loglik_LOPO_noblock_noreward', 'loglik_LOPO_block_noreward', 'loglik_LOPO_block_reward', 'loglik_LOPO_block_noreward_sess', 'loglik_LOTO_noblock_noreward', 'loglik_LOTO_block_noreward', 'loglik_LOTO_block_reward', 'loglik_LOTO_block_noreward_sess');

%% model comparison statistics reported in the paper
Model1_LOPO_LL = sum(loglik_LOPO_block_noreward(blockindices == 1)) + sum(loglik_LOPO_block_reward(blockindices == 2)); 
Model2_LOPO_LL = sum(loglik_LOPO_block_reward); 
Model3_LOPO_LL = sum(loglik_LOPO_block_noreward); 
Model4_LOPO_LL = sum(loglik_LOPO_noblock_noreward); 
Model6_LOPO_LL = sum(loglik_LOPO_block_noreward_sess); 

Model1_LOTO_LL = sum(loglik_LOTO_block_noreward(blockindices == 1)) + sum(loglik_LOTO_block_reward(blockindices == 2)); 
Model2_LOTO_LL = sum(loglik_LOTO_block_reward); 
Model3_LOTO_LL = sum(loglik_LOTO_block_noreward); 
Model4_LOTO_LL = sum(loglik_LOTO_noblock_noreward); 
Model6_LOTO_LL = sum(loglik_LOTO_block_noreward_sess); 

[Model1_LOPO_LL, Model2_LOPO_LL, Model3_LOPO_LL, Model4_LOPO_LL, Model6_LOPO_LL]
[Model1_LOTO_LL, Model2_LOTO_LL, Model3_LOTO_LL, Model4_LOTO_LL, Model6_LOTO_LL]

%% Winning model is Model 3. Code below can be used to visualize the smooth model fit, as shown in Fig. 3B in the paper. 
for blockidx = 1:2
	choice_p = gp(hyp_block_noreward{blockidx}, inffunc, meanfunc, covfunc, likfunc, beadsdif(blockindices == blockidx), betchoice(blockindices == blockidx), [-12:0.1:12]');
	figure(1); subplot(2, 1, blockidx); plot(-12:0.1:12, choice_p/2+0.5, '-');
end

end
