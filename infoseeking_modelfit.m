function infoseeking_modelfit
%% Model fit on info-seeking behavior, i.e., whether to draw at least one additional bead, as a function of beads difference and reward asymmetry.
%% Use gaussian process logistic regression to estimate underlying subjective value of information (SVOI) function.
%% Fit the winning model in infoseeking_modelcomp.m (Model 3), visualize its fit with population average and standard error estimated by bootstrap (Fig. 3A in the paper), and visualize latent SVOI function (Fig. 4A; also used in the fMRI analysis). 

%% this uses GPML toolbox (Rasmussen & Nickisch, 2010; Rasmussen & Williams, 2006), available at www.gaussianprocess.org/gpml/code/matlab/doc/ and github (github.com/alshedivat/gpml).

%% written by Kenji Kobayashi

% GPML toolbox setup. Note that you first need to have GPML toolbox.
gpmldir = 'gpml-matlab-master/';
run([gpmldir, 'startup.m'])
meanfunc = @meanConst;
covfunc = @covSEiso;
likfunc = @likLogistic;
inffunc = @infVB;
numeval = -200;

%% load and concatenate all participants' data into vectors
datadir = 'behavdata/';
subnames = dir([datadir, '*.mat']);
conc_blockindices = [];
conc_beadsdif = [];
conc_rew_corr_H = [];
conc_firstdraw = [];
subindices = [];

for sub = 1:length(subnames)
	load([datadir, subnames(sub).name], 'gameindices', 'init_beads_H', 'init_beads_L', 'rew_corr_H', 'info');

	% extract info-seeking trials only
	info_trial = info(:, 1) ~= 0;							% if nonzero, participants were presented with the forced extra bead (i.e., info-seeking trial)
	firstdraw = 2 * (info(info_trial, 2) ~= 0) - 1;			% if 1, participants drew at least one bead after the forced extra bead, if -1, they did not
	gameindices = gameindices(info_trial);					% 1 (baseline block) or 2 (scale block)
	rew_corr_H = rew_corr_H(info_trial);					% 70 or 170 in baseline block, 70 or 7 in scale block

	% the number of beads presented before info-seeking choices, taking into account the forced extra bead
	beads_H = init_beads_H(info_trial) + (info(info_trial, 1) == 1);
	beads_L = init_beads_L(info_trial) + (info(info_trial, 1) == 2);
	beadsdif = beads_H - beads_L;							% from -11 to 11, 2 increment

	conc_blockindices = [conc_blockindices; gameindices];
	conc_beadsdif = [conc_beadsdif; beadsdif];
	conc_rew_corr_H = [conc_rew_corr_H; rew_corr_H];
	conc_firstdraw = [conc_firstdraw; firstdraw];

	subindices = [subindices; sub*ones(length(firstdraw), 1)];	% participant idx for bootstrap
end

blockindices = conc_blockindices;
rewindices = conc_rew_corr_H == 70;	%% whether the high reward is 70 or not (may be 170 or 7)
beadsdif = conc_beadsdif;
firstdraw = conc_firstdraw;

pred_beadsdif = -12:0.1:12;	%% x axis of the smooth model fit
beadsdiflevels = -11:2:11;
bootiternum = 5000;

colorblock = [80, 123, 0; 123, 68, 37]/255;	% first row: green for baseline block, second row: brown for scale block

for blockidx = 1:2	% baseline block / scale block

	%% model fitting
	hyp.mean = 0; hyp.cov = [0; 0];
	hyp = minimize(hyp, @gp, numeval, inffunc, meanfunc, covfunc, likfunc, beadsdif(blockindices == blockidx), firstdraw(blockindices == blockidx));
	[draw_phat, ~, SVOI, SVOI_var] = gp(hyp, inffunc, meanfunc, covfunc, likfunc, beadsdif(blockindices == blockidx), firstdraw(blockindices == blockidx), pred_beadsdif');
	save(['infoseeking_modelfit_block', num2str(blockidx), '.mat'], 'draw_phat', 'SVOI', 'pred_beadsdif');

	%% descriptive statistics of observed behavior (population mean and bootstrap SE for each reward condition)
	populmean = zeros(2, length(beadsdiflevels));
	populse = zeros(2, length(beadsdiflevels));

	for rewidx = 0:1	%% 70 or not

		% compute average and SE for each level of beads difference
		for dif = beadsdiflevels
			drawdata = 0.5 * firstdraw(blockindices == blockidx & beadsdif == dif & rewindices == rewidx) + 0.5;	%% recode, 1: draw, 0: not draw
			subdata = subindices(blockindices == blockidx & beadsdif == dif & rewindices == rewidx);

			% average
			populmean(rewidx + 1, find(beadsdiflevels == dif)) = mean(drawdata);

			% bootstrap SEM. resample participants (only those that have non-zero trials in the specific beads difference and reward condition)
			nonzerosub = unique(subdata);
			bootmean = zeros(bootiternum, 1);
			parfor bootiter = 1:bootiternum
				bootsub = randi(length(nonzerosub), length(nonzerosub), 1);
				bootdata = [];
				for sub = 1:length(nonzerosub)
					bootdata = [bootdata; drawdata(subdata == nonzerosub(bootsub(sub)))];
				end
				bootmean(bootiter) = mean(bootdata);
			end
			populse(rewidx + 1, find(beadsdiflevels == dif)) = std(bootmean);
		end
	end

	%% visualize observed behavior and model fit
	figure(1); subplot(2, 1, blockidx); hold on;
	plot(pred_beadsdif, draw_phat/2+0.5, '-', 'Color', colorblock(blockidx, :), 'LineWidth', 2);	%% model fit

	offset = 0.05;
	plot([beadsdiflevels+offset; beadsdiflevels+offset], [populmean(1, :) + populse(1, :); populmean(1, :) - populse(1, :)], '-', 'Color', colorblock(blockidx, :), 'LineWidth', 1);
	plot([beadsdiflevels-offset; beadsdiflevels-offset], [populmean(2, :) + populse(2, :); populmean(2, :) - populse(2, :)], '-', 'Color', colorblock(blockidx, :), 'LineWidth', 1);
	plot(beadsdiflevels+offset, populmean(1, :), 'd', 'MarkerEdgeColor', colorblock(blockidx, :), 'MarkerFaceColor', 'w', 'MarkerSize', 8, 'LineWidth', 1);
	plot(beadsdiflevels-offset, populmean(2, :), 'o', 'MarkerEdgeColor', colorblock(blockidx, :), 'MarkerFaceColor', colorblock(blockidx, :), 'MarkerSize', 8, 'LineWidth', 1);

	xlim([-12, 12]); ylim([0, 1]); set(gca, 'XTick', -12:6:12, 'YTick', 0:0.5:1);

	%% visualize latent SVOI function and its SE
	figure(2); hold on;
	h = fill([pred_beadsdif'; pred_beadsdif(end:-1:1)'], [SVOI + sqrt(SVOI_var); SVOI(end:-1:1) - sqrt(SVOI_var(end:-1:1))], colorblock(blockidx, :));
	set(h, 'FaceAlpha', 0.5);
	plot(pred_beadsdif, SVOI, '-', 'Color', colorblock(blockidx, :), 'LineWidth', 1);

	xlim([-12, 12]); ylim([-3, 1]); set(gca, 'XTick', -12:6:12, 'YTick', -3:1:1);
end

saveas(1, 'infoseeking_modelfit.pdf');
saveas(2, 'infoseeking_modelfit_SVOI.pdf');
close(1);
close(2);

end
