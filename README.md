This is behavioral data and analysis code published in the following paper:

Kobayashi, K., Lee, S., Filipowicz, A. L. S., McGaughey, K. D., Kable, J. W. & Nassar, M. R. Dynamic representation of the subjective value of information. bioRxiv, https://doi.org/10.1101/2021.02.12.431038.

Please see the paper for the details of experimental paradigm and analysis.

## Data
Each file (MATLAB data file or CSV) under behavdata/ contains behavioral data from each participant. Trials from all four scanning runs have been concatenated into vectors or matrices.

Files contain the following variables.
- gameindices: whether the trial is in the baseline block (1) or the scale block (2).
- sessionindices: whether the trial is in the 1st or 2nd scanning run within each block.
- init\_beads\_H: the number of high-reward beads presented in the initial screen.
- init\_beads\_L: the number of low-reward beads presented in the initial screen.
- rew\_corr\_H: reward for a correct bet on the high-reward jar, either 70 or 170 in the baseline block and 70 or 7 in the scale block. randomized across trials.
- rew\_corr\_L: reward for a correct bet on the low-reward jar, either 10 or 110 in the baseline block and 10 or 1 in the scale block. (Redundant with rew\_corr\_H.)
- rew\_incorr: reward for an incorrect bet, either 0 or 100 in the baseline block; always 0 in the scale block. (Redundant with rew\_corr\_H.)
- choice: whether the participant bet on the high-reward jar (1) or low-reward jar (2).
- info: additional draws observed in each trial. 1: high-reward bead, 2: low-reward bead. The first column indicates the extra bead forced in all info-seeking trials, and the remaining columns indicate the additional beads volitionally drawn by the participant. this matrix is zero-padded in the second dimension; all entries are zero for bet-only trials (where no additional beads were observed), only the first column is non-zero for info-seeking trials where the participant did not draw any additional bead (after the forced extra bead), only the first and second columns are non-zero for trials where the participant only drew one additional bead (after the forced extra bead), and so on. The size of the second dimension (or the number of columns in CSV files) thus depends on the maximum draws made by each participant.
- LRside\_H: whether the high-reward jar was presented to the left (1) or right (2). This was counterbalanced across scanning runs, which order was counterbalanced across participants. Note that the left jar was always a face-majority jar and the right jar was a house-majority jar.

## Code
Model fit and comparison was conducted using Gaussian processes logistic regression.
- infochoice\_modelcomp.m and betchoice\_modelcomp.m conduct model comparison on info-seeking and betting behavior, respectively, using leave-one-participant-out and leave-one-trial-out cross validation. They take hours to complete, but the outputs are available in corresponding MAT files.
- infochoice\_modelfit.m and betchoice\_modelfit.m conduct model fit (using the winning model chosen by model comparison above) and visualize it along with observed behavior. The outputs (figures and model fit) are available in corresponding PDF and MAT files.

The scripts require GPML toolbox (Rasmussen & Nickisch, 2010; Rasmussen & Williams, 2006), available at www.gaussianprocess.org/gpml/code/matlab/doc/ and GitHub (github.com/alshedivat/gpml).
