function infoseeking_modelcomp
%% Model fit and comparison on info-seeking behavior, i.e., whether to draw at least one additional bead, as a function of beads difference and reward asymmetry.
%% Use gaussian process logistic regression to estimate underlying subjective value of information (SVOI) function.
%% Model comparison uses cross validation, leave-one-participant-out (LOPO) and leave-one-trial-out (LOTO). Visualization of model fit at the end.

%% this uses GPML toolbox (Rasmussen & Nickisch, 2010; Rasmussen & Williams, 2006), available at www.gaussianprocess.org/gpml/code/matlab/doc/ and github (github.com/alshedivat/gpml).

% In the paper, we compared the following models:
%%  Model 1: sensitivity to reward manipulation in scale block but not in baseline block
%%  Model 2: sensitivity to reward manipulation in both baseline and scale blocks
%%  Model 3: no sensitivity to reward manipulation in baseline or scale block, but different SVOI between blocks
%%  Model 4: no sensitivity to reward manipulation, same SVOI for both blocks
%%  Model 5: same as Model 3, except that the SVOI is symmetric w.r.t. beads difference
%%  Model 6: same as Model 3, except that the SVOI is different between the first and second scanning runs

%% written by Kenji Kobayashi

% GPML toolbox setup. Note that you first need to have GPML toolbox.
gpmldir = 'gpml-matlab-master/';
run([gpmldir, 'startup.m'])
meanfunc = @meanConst;
covfunc = @covSEiso;
likfunc = @likLogistic;
inffunc = @infVB;
numeval = -200;

%% load and concatenate all participants' data into vectors
datadir = 'behavdata/';
subnames = dir([datadir, '*.mat']);
conc_blockindices = [];
conc_sessionindices = [];
conc_beadsdif = [];
conc_rew_corr_H = [];
conc_firstdraw = [];
subindices = [];

for sub = 1:length(subnames)
	load([datadir, subnames(sub).name], 'gameindices', 'sessionindices', 'init_beads_H', 'init_beads_L', 'rew_corr_H', 'info');

	% extract info-seeking trials only
	info_trial = info(:, 1) ~= 0;							% if nonzero, participants were presented with the forced extra bead (i.e., info-seeking trial)
	firstdraw = 2 * (info(info_trial, 2) ~= 0) - 1;			% if 1, participants drew at least one bead after the forced extra bead, if -1, they did not
	gameindices = gameindices(info_trial);					% 1 (baseline block) or 2 (scale block)
	sessionindices = sessionindices(info_trial);				% 1 or 2, scanning run within block
	rew_corr_H = rew_corr_H(info_trial);					% 70 or 170 in baseline block, 70 or 7 in scale block

	% the number of beads presented before info-seeking choices, taking into account the forced extra bead
	beads_H = init_beads_H(info_trial) + (info(info_trial, 1) == 1);
	beads_L = init_beads_L(info_trial) + (info(info_trial, 1) == 2);
	beadsdif = beads_H - beads_L;							% from -11 to 11, 2 increment

	conc_blockindices = [conc_blockindices; gameindices];
	conc_sessionindices = [conc_sessionindices; sessionindices];
	conc_beadsdif = [conc_beadsdif; beadsdif];
	conc_rew_corr_H = [conc_rew_corr_H; rew_corr_H];
	conc_firstdraw = [conc_firstdraw; firstdraw];

	subindices = [subindices; sub*ones(length(firstdraw), 1)];	% participant idx for leave-one-participant-out CV, from 1 to 15
end

blockindices = conc_blockindices;
sessindices = conc_sessionindices;
rewindices = conc_rew_corr_H == 70;	%% whether the high reward is 70 or not (may be 170 or 7)
beadsdif = conc_beadsdif;
firstdraw = conc_firstdraw;

%% model comparison 
% To evaluate Models 1 to 4, compute CV log likelihood for each trial with 1) block-agnostic, reward-agnostic function, 2) block-specific, reward-agnostic function, and 3) block-specific, reward-specific latent value function
% To evaluate Model 5, compute CV with 4) block-specific, reward-agnostic, symmetric function, implemented as the logistic regression w.r.t. absolute value of beads difference
% To evaluate Model 6, compute CV with 5) session-specific, reward-agnostic function
% For each trial, CV log likelihood for each model is computed for LOPO and LOTO

%% before CV, estimate hyperparameters using all data
hyp_noblock_noreward.mean = 0; hyp_noblock_noreward.cov = [0; 0];
hyp_noblock_noreward = minimize(hyp_noblock_noreward, @gp, numeval, inffunc, meanfunc, covfunc, likfunc, beadsdif, firstdraw);

for blockidx = 1:2	% trials in which block to evaluate

	hyp_block_noreward{blockidx}.mean = 0; hyp_block_noreward{blockidx}.cov = [0; 0];
	hyp_block_noreward{blockidx} = minimize(hyp_block_noreward{blockidx}, @gp, numeval, inffunc, meanfunc, covfunc, likfunc, beadsdif(blockindices == blockidx), firstdraw(blockindices == blockidx));

	hyp_block_noreward_symm{blockidx}.mean = 0; hyp_block_noreward_symm{blockidx}.cov = [0; 0];
	hyp_block_noreward_symm{blockidx} = minimize(hyp_block_noreward_symm{blockidx}, @gp, numeval, inffunc, meanfunc, covfunc, likfunc, abs(beadsdif(blockindices == blockidx)), firstdraw(blockindices == blockidx));

	for rewidx = 0:1	% trials in which reward condition to evaluate, 70 or not
		hyp_block_reward{blockidx, rewidx+1}.mean = 0; hyp_block_reward{blockidx, rewidx+1}.cov = [0; 0];
		hyp_block_reward{blockidx, rewidx+1} = minimize(hyp_block_reward{blockidx, rewidx+1}, @gp, numeval, inffunc, meanfunc, covfunc, likfunc, beadsdif(blockindices == blockidx & rewindices == rewidx), firstdraw(blockindices == blockidx & rewindices == rewidx));
	end

	for sessidx = 1:2	% trials from which scanning run to evaluate in CV
		hyp_block_noreward_sess{blockidx, sessidx}.mean = 0; hyp_block_noreward_sess{blockidx, sessidx}.cov = [0; 0];
		hyp_block_noreward_sess{blockidx, sessidx} = minimize(hyp_block_noreward_sess{blockidx, sessidx}, @gp, numeval, inffunc, meanfunc, covfunc, likfunc, beadsdif(blockindices == blockidx & sessindices == sessidx), firstdraw(blockindices == blockidx & sessindices == sessidx));
	end
end


%% LOPO
loglik_LOPO_noblock_noreward = zeros(length(firstdraw), 1);
loglik_LOPO_block_noreward = zeros(length(firstdraw), 1);
loglik_LOPO_block_reward = zeros(length(firstdraw), 1);
loglik_LOPO_block_noreward_symm = zeros(length(firstdraw), 1);
loglik_LOPO_block_noreward_sess = zeros(length(firstdraw), 1);

% leave out data from one participant, train the model using the rest, and test the model on the left-out participant data
for leave_subidx = 1:max(subindices)	%% which participant to leave out

	% training data
	train_blockindices = blockindices(subindices ~= leave_subidx);
	train_sessindices = sessindices(subindices ~= leave_subidx);
	train_rewindices = rewindices(subindices ~= leave_subidx);
	train_beadsdif = beadsdif(subindices ~= leave_subidx);
	train_firstdraw = firstdraw(subindices ~= leave_subidx);

	% testing data
	test_blockindices = blockindices(subindices == leave_subidx);
	test_sessindices = sessindices(subindices == leave_subidx);
	test_rewindices = rewindices(subindices == leave_subidx);
	test_beadsdif = beadsdif(subindices == leave_subidx);
	test_firstdraw = firstdraw(subindices == leave_subidx);

	% block-agnostic, reward-agnostic model
	hyp_noblock_noreward_CV = minimize(hyp_noblock_noreward, @gp, numeval, inffunc, meanfunc, covfunc, likfunc, train_beadsdif, train_firstdraw);
	[~, ~, ~, ~, loglik_LOPO_noblock_noreward(subindices == leave_subidx)] = gp(hyp_noblock_noreward_CV, inffunc, meanfunc, covfunc, likfunc, train_beadsdif, train_firstdraw, test_beadsdif, test_firstdraw);

	for blockidx = 1:2	% trials in which block to evaluate

		% block-specific, reward-agnostic model
		hyp_block_noreward_CV = minimize(hyp_block_noreward{blockidx}, @gp, numeval, inffunc, meanfunc, covfunc, likfunc, train_beadsdif(train_blockindices == blockidx), train_firstdraw(train_blockindices == blockidx));
		[~, ~, ~, ~, loglik_LOPO_block_noreward(subindices == leave_subidx & blockindices == blockidx)] = gp(hyp_block_noreward_CV, inffunc, meanfunc, covfunc, likfunc, train_beadsdif(train_blockindices == blockidx), train_firstdraw(train_blockindices == blockidx), test_beadsdif(test_blockindices == blockidx), test_firstdraw(test_blockindices == blockidx));

		% block-specific, reward-agnostic, symmetric model
		hyp_block_noreward_symm_CV = minimize(hyp_block_noreward_symm{blockidx}, @gp, numeval, inffunc, meanfunc, covfunc, likfunc, abs(train_beadsdif(train_blockindices == blockidx)), train_firstdraw(train_blockindices == blockidx));
		[~, ~, ~, ~, loglik_LOPO_block_noreward_symm(subindices == leave_subidx & blockindices == blockidx)] = gp(hyp_block_noreward_symm_CV, inffunc, meanfunc, covfunc, likfunc, abs(train_beadsdif(train_blockindices == blockidx)), train_firstdraw(train_blockindices == blockidx), abs(test_beadsdif(test_blockindices == blockidx)), test_firstdraw(test_blockindices == blockidx));

		for rewidx = 0:1	% trials in which reward condition to evaluate, 70 or not
			% block-specific, reward-specific model
			hyp_block_reward_CV = minimize(hyp_block_reward{blockidx, rewidx+1}, @gp, numeval, inffunc, meanfunc, covfunc, likfunc, train_beadsdif(train_blockindices == blockidx & train_rewindices == rewidx), train_firstdraw(train_blockindices == blockidx & train_rewindices == rewidx));
			[~, ~, ~, ~, loglik_LOPO_block_reward(subindices == leave_subidx & blockindices == blockidx & rewindices == rewidx)] = gp(hyp_block_reward_CV, inffunc, meanfunc, covfunc, likfunc, train_beadsdif(train_blockindices == blockidx & train_rewindices == rewidx), train_firstdraw(train_blockindices == blockidx & train_rewindices == rewidx), test_beadsdif(test_blockindices == blockidx & test_rewindices == rewidx), test_firstdraw(test_blockindices == blockidx & test_rewindices == rewidx));
		end

		for sessidx = 1:2	% trials from which scanning run to evaluate in CV
			% block-specific, reward-agnostic, session-wise model
			hyp_block_noreward_sess_CV = minimize(hyp_block_noreward_sess{blockidx, sessidx}, @gp, numeval, inffunc, meanfunc, covfunc, likfunc, train_beadsdif(train_blockindices == blockidx & train_sessindices == sessidx), train_firstdraw(train_blockindices == blockidx & train_sessindices == sessidx));
			[~, ~, ~, ~, loglik_LOPO_block_noreward_sess(subindices == leave_subidx & blockindices == blockidx & sessindices == sessidx)] = gp(hyp_block_noreward_sess_CV, inffunc, meanfunc, covfunc, likfunc, train_beadsdif(train_blockindices == blockidx & train_sessindices == sessidx), train_firstdraw(train_blockindices == blockidx & train_sessindices == sessidx), test_beadsdif(test_blockindices == blockidx & test_sessindices == sessidx), test_firstdraw(test_blockindices == blockidx & test_sessindices == sessidx));
		end
	end
end


%% LOTO
loglik_LOTO_noblock_noreward = zeros(length(firstdraw), 1);
loglik_LOTO_block_noreward = zeros(length(firstdraw), 1);
loglik_LOTO_block_reward = zeros(length(firstdraw), 1);
loglik_LOTO_block_noreward_symm = zeros(length(firstdraw), 1);
loglik_LOTO_block_noreward_sess = zeros(length(firstdraw), 1);

% leave out one trial, train the model using the rest, and test the model on the left-out trial data
% to speed up the computation, just leave out one (first) trial for each unique combination of experiment parameters that are relevant to the model being evaluated (draw choice, beads difference, block, reward condition, or session)
% for instance, for the model that assumes a block-specific but reward-agnostic value function, one trial from the baseline (or scale) block is left out, the model is trained on the remaining trials in the baseline (or scale) block, and the trained model is tested on the left-out trial

for leave_firstdraw = [-1, 1]	% drawing choice of the left-out trial
	for leave_beadsdif = -11:2:11	% beadsdif of the left-out trial

		% block-agnostic, reward-agnostic model
		leavetrialidx = find(beadsdif == leave_beadsdif & firstdraw == leave_firstdraw);
		if ~isempty(leavetrialidx)

			traintrialidx = [1:leavetrialidx(1)-1, leavetrialidx(1)+1:length(firstdraw)];
			train_beadsdif = beadsdif(traintrialidx);
			train_firstdraw = firstdraw(traintrialidx);

			hyp_noblock_noreward_CV = minimize(hyp_noblock_noreward, @gp, numeval, inffunc, meanfunc, covfunc, likfunc, train_beadsdif, train_firstdraw);
			[~, ~, ~, ~, loglik_LOTO_noblock_noreward(leavetrialidx)] = gp(hyp_noblock_noreward_CV, inffunc, meanfunc, covfunc, likfunc, train_beadsdif, train_firstdraw, leave_beadsdif, leave_firstdraw);
		end

		for leave_blockidx = 1:2	% block of the left-out trial

			% block-specific, reward-agnostic model
			leavetrialidx = find(blockindices == leave_blockidx & beadsdif == leave_beadsdif & firstdraw == leave_firstdraw);
			if ~isempty(leavetrialidx)

				traintrialidx = [1:leavetrialidx(1)-1, leavetrialidx(1)+1:length(firstdraw)];
				train_blockindices = blockindices(traintrialidx);
				train_beadsdif = beadsdif(traintrialidx);
				train_firstdraw = firstdraw(traintrialidx);

				hyp_block_noreward_CV = minimize(hyp_block_noreward{leave_blockidx}, @gp, numeval, inffunc, meanfunc, covfunc, likfunc, train_beadsdif(train_blockindices == leave_blockidx), train_firstdraw(train_blockindices == leave_blockidx));
				[~, ~, ~, ~, loglik_LOTO_block_noreward(leavetrialidx)] = gp(hyp_block_noreward_CV, inffunc, meanfunc, covfunc, likfunc, train_beadsdif(train_blockindices == leave_blockidx), train_firstdraw(train_blockindices == leave_blockidx), leave_beadsdif, leave_firstdraw);
			end

			for leave_rewidx = 0:1	% reward condition of the left-out trial, 70 or not

				% block-specific, reward-specific model
				leavetrialidx = find(blockindices == leave_blockidx & rewindices == leave_rewidx & beadsdif == leave_beadsdif & firstdraw == leave_firstdraw);
				if ~isempty(leavetrialidx)

					traintrialidx = [1:leavetrialidx(1)-1, leavetrialidx(1)+1:length(firstdraw)];
					train_blockindices = blockindices(traintrialidx);
					train_rewindices = rewindices(traintrialidx);
					train_beadsdif = beadsdif(traintrialidx);
					train_firstdraw = firstdraw(traintrialidx);

					hyp_block_reward_CV = minimize(hyp_block_reward{leave_blockidx, leave_rewidx+1}, @gp, numeval, inffunc, meanfunc, covfunc, likfunc, train_beadsdif(train_blockindices == leave_blockidx & train_rewindices == leave_rewidx), train_firstdraw(train_blockindices == leave_blockidx & train_rewindices == leave_rewidx));
					[~, ~, ~, ~, loglik_LOTO_block_reward(leavetrialidx)] = gp(hyp_block_reward_CV, inffunc, meanfunc, covfunc, likfunc, train_beadsdif(train_blockindices == leave_blockidx & train_rewindices == leave_rewidx), train_firstdraw(train_blockindices == leave_blockidx & train_rewindices == leave_rewidx), leave_beadsdif, leave_firstdraw);
				end
			end

			for leave_sessidx = 1:2	% scanning run of the left-out trial

				% block-specific, reward-specific, session-wise model
				leavetrialidx = find(blockindices == leave_blockidx & sessindices == leave_sessidx & beadsdif == leave_beadsdif & firstdraw == leave_firstdraw);
				if ~isempty(leavetrialidx)

					traintrialidx = [1:leavetrialidx(1)-1, leavetrialidx(1)+1:length(firstdraw)];
					train_blockindices = blockindices(traintrialidx);
					train_sessindices = sessindices(traintrialidx);
					train_beadsdif = beadsdif(traintrialidx);
					train_firstdraw = firstdraw(traintrialidx);

					hyp_block_noreward_sess_CV = minimize(hyp_block_noreward_sess{leave_blockidx, leave_sessidx}, @gp, numeval, inffunc, meanfunc, covfunc, likfunc, train_beadsdif(train_blockindices == leave_blockidx & train_sessindices == leave_sessidx), train_firstdraw(train_blockindices == leave_blockidx & train_sessindices == leave_sessidx));
					[~, ~, ~, ~, loglik_LOTO_block_noreward_sess(leavetrialidx)] = gp(hyp_block_noreward_sess_CV, inffunc, meanfunc, covfunc, likfunc, train_beadsdif(train_blockindices == leave_blockidx & train_sessindices == leave_sessidx), train_firstdraw(train_blockindices == leave_blockidx & train_sessindices == leave_sessidx), leave_beadsdif, leave_firstdraw);
				end
			end
		end
	end

	% block-specific, reward-agnostic, symmetric model
	for leave_beadsdif = 1:2:11	% beadsdif of the left-out trial, positive value only
		for leave_blockidx = 1:2	% block of the left-out trial

			leavetrialidx = find(blockindices == leave_blockidx & abs(beadsdif) == leave_beadsdif & firstdraw == leave_firstdraw);
			if ~isempty(leavetrialidx)

				traintrialidx = [1:leavetrialidx(1)-1, leavetrialidx(1)+1:length(firstdraw)];
				train_blockindices = blockindices(traintrialidx);
				train_rewindices = rewindices(traintrialidx);
				train_beadsdif = beadsdif(traintrialidx);
				train_firstdraw = firstdraw(traintrialidx);

				hyp_block_noreward_symm_CV = minimize(hyp_block_noreward_symm{leave_blockidx}, @gp, numeval, inffunc, meanfunc, covfunc, likfunc, abs(train_beadsdif(train_blockindices == leave_blockidx)), train_firstdraw(train_blockindices == leave_blockidx));
				[~, ~, ~, ~, loglik_LOTO_block_noreward_symm(leavetrialidx)] = gp(hyp_block_noreward_symm_CV, inffunc, meanfunc, covfunc, likfunc, abs(train_beadsdif(train_blockindices == leave_blockidx)), train_firstdraw(train_blockindices == leave_blockidx), abs(leave_beadsdif), leave_firstdraw);

			end
		end
	end
end

save('infoseeking_modelcomp.mat', 'loglik_LOPO_noblock_noreward', 'loglik_LOPO_block_noreward', 'loglik_LOPO_block_reward', 'loglik_LOPO_block_noreward_symm', 'loglik_LOPO_block_noreward_sess', 'loglik_LOTO_noblock_noreward', 'loglik_LOTO_block_noreward', 'loglik_LOTO_block_reward', 'loglik_LOTO_block_noreward_symm', 'loglik_LOTO_block_noreward_sess');


%% model comparison statistics reported in the paper
Model1_LOPO_LL = sum(loglik_LOPO_block_noreward(blockindices == 1)) + sum(loglik_LOPO_block_reward(blockindices == 2)); 
Model2_LOPO_LL = sum(loglik_LOPO_block_reward); 
Model3_LOPO_LL = sum(loglik_LOPO_block_noreward); 
Model4_LOPO_LL = sum(loglik_LOPO_noblock_noreward); 
Model5_LOPO_LL = sum(loglik_LOPO_block_noreward_symm); 
Model6_LOPO_LL = sum(loglik_LOPO_block_noreward_sess); 

Model1_LOTO_LL = sum(loglik_LOTO_block_noreward(blockindices == 1)) + sum(loglik_LOTO_block_reward(blockindices == 2)); 
Model2_LOTO_LL = sum(loglik_LOTO_block_reward); 
Model3_LOTO_LL = sum(loglik_LOTO_block_noreward); 
Model4_LOTO_LL = sum(loglik_LOTO_noblock_noreward); 
Model5_LOTO_LL = sum(loglik_LOTO_block_noreward_symm); 
Model6_LOTO_LL = sum(loglik_LOTO_block_noreward_sess); 

[Model1_LOPO_LL, Model2_LOPO_LL, Model3_LOPO_LL, Model4_LOPO_LL, Model5_LOPO_LL, Model6_LOPO_LL]
[Model1_LOTO_LL, Model2_LOTO_LL, Model3_LOTO_LL, Model4_LOTO_LL, Model5_LOTO_LL, Model6_LOTO_LL]


%% test for asymmetry of SVOI in each block
Model3_LOPO_LL_block1 = sum(loglik_LOPO_block_noreward(blockindices == 1)); 
Model3_LOPO_LL_block2 = sum(loglik_LOPO_block_noreward(blockindices == 2)); 
Model5_LOPO_LL_block1 = sum(loglik_LOPO_block_noreward_symm(blockindices == 1)); 
Model5_LOPO_LL_block2 = sum(loglik_LOPO_block_noreward_symm(blockindices == 2)); 

Model3_LOTO_LL_block1 = sum(loglik_LOTO_block_noreward(blockindices == 1)); 
Model3_LOTO_LL_block2 = sum(loglik_LOTO_block_noreward(blockindices == 2)); 
Model5_LOTO_LL_block1 = sum(loglik_LOTO_block_noreward_symm(blockindices == 1)); 
Model5_LOTO_LL_block2 = sum(loglik_LOTO_block_noreward_symm(blockindices == 2)); 

[Model3_LOPO_LL_block1, Model5_LOPO_LL_block1]
[Model3_LOPO_LL_block2, Model5_LOPO_LL_block2]
[Model3_LOTO_LL_block1, Model5_LOTO_LL_block1]
[Model3_LOTO_LL_block2, Model5_LOTO_LL_block2]
 

%% Winning model is Model 3. Code below can be used to visualize the smooth model fit and latent subjective VOI, as shown in Figs. 3A and 4A in the paper. 
for blockidx = 1:2
	[info_p, ~, SVOI, SVOI_var] = gp(hyp_block_noreward{blockidx}, inffunc, meanfunc, covfunc, likfunc, beadsdif(blockindices == blockidx), firstdraw(blockindices == blockidx), [-12:0.1:12]');
	figure(1); subplot(2, 1, blockidx); plot(-12:0.1:12, info_p/2+0.5, '-');
	figure(2); subplot(2, 1, blockidx); hold on;
	fill([[-12:0.1:12]'; [12:-0.1:-12]'], [SVOI + sqrt(SVOI_var); SVOI(end:-1:1) - sqrt(SVOI_var(end:-1:1))]);
	plot(-12:0.1:12, SVOI, '-');
end

end
