function betchoice_modelfit
%% Model fit on betting behavior in bet-only trials, i.e., whether to bet on the high-reward or low-reward jar, as a function of beads difference.
%% Use gaussian process logistic regression to estimate underlying utility function.
%% Fit the winning model in betseeking_modelcomp.m (Model 3) and visualize its fit with population average and standard error estimated by bootstrap (Fig. 3B in the paper).

%% this uses GPML toolbox (Rasmussen & Nickisch, 2010; Rasmussen & Williams, 2006), available at www.gaussianprocess.org/gpml/code/matlab/doc/ and github (github.com/alshedivat/gpml).

%% written by Kenji Kobayashi

% GPML toolbox setup. Note that you first need to have GPML toolbox.
gpmldir = 'gpml-matlab-master/';
% run([gpmldir, 'startup.m'])
meanfunc = @meanConst;
covfunc = @covSEiso;
likfunc = @likLogistic;
inffunc = @infVB;
numeval = -200;

%% load and concatenate all participants' data into vectors
datadir = 'behavdata/';
subnames = dir([datadir, '*.mat']);
conc_blockindices = [];
conc_beadsdif = [];
conc_rew_corr_H = [];
conc_betchoice = [];
subindices = [];

for sub = 1:length(subnames)
	load([datadir, subnames(sub).name], 'gameindices', 'init_beads_H', 'init_beads_L', 'rew_corr_H', 'info', 'choice');

	% extract bet-only trials only
	bet_trial = info(:, 1) == 0;								% if zero, participants could not draw additional beads (i.e., bet-only trial)
	gameindices = gameindices(bet_trial);						% 1 (baseline block) or 2 (scale block)
	rew_corr_H = rew_corr_H(bet_trial);						% 70 or 170 in baseline block, 70 or 7 in scale block
	choice = choice(bet_trial);								% 1: bet on the high-reward jar, 2: bet on the low-reward jar

	% the number of beads presented before a bet
	beadsdif = init_beads_H(bet_trial) - init_beads_L(bet_trial);	% from -10 to 10, 2 increment

	conc_blockindices = [conc_blockindices; gameindices];
	conc_beadsdif = [conc_beadsdif; beadsdif];
	conc_rew_corr_H = [conc_rew_corr_H; rew_corr_H];
	conc_betchoice = [conc_betchoice; choice];

	subindices = [subindices; sub*ones(length(choice), 1)];	% participant idx for bootstrap
end

blockindices = conc_blockindices;
rewindices = conc_rew_corr_H == 70;	%% whether the high reward is 70 or not (may be 170 or 7)
beadsdif = conc_beadsdif;
betchoice = -2 * conc_betchoice + 3;	%% 1: bet on the high-reward jar, -1: bet on the low-reward jar

pred_beadsdif = -12:0.1:12;	%% x axis of the smooth model fit
beadsdiflevels = -10:2:10;
bootiternum = 5000;

colorblock = [80, 123, 0; 123, 68, 37]/255;	% first row: green for baseline block, second row: brown for scale block

for blockidx = 1:2	% baseline block / scale block

	%% model fitting
	hyp.mean = 0; hyp.cov = [0; 0];
	hyp = minimize(hyp, @gp, numeval, inffunc, meanfunc, covfunc, likfunc, beadsdif(blockindices == blockidx), betchoice(blockindices == blockidx));
	[choice_phat, ~, value] = gp(hyp, inffunc, meanfunc, covfunc, likfunc, beadsdif(blockindices == blockidx), betchoice(blockindices == blockidx), pred_beadsdif');
	save(['betchoice_modelfit_block', num2str(blockidx), '.mat'], 'choice_phat', 'value', 'pred_beadsdif');

	%% descriptive statistics of observed behavior (population mean and bootstrap SE for each reward condition)
	populmean = zeros(2, length(beadsdiflevels));
	populse = zeros(2, length(beadsdiflevels));

	for rewidx = 0:1	%% 70 or not

		% compute average and SE for each level of beads difference
		for dif = beadsdiflevels
			betdata = 0.5 * betchoice(blockindices == blockidx & beadsdif == dif & rewindices == rewidx) + 0.5;	%% recode, 1: bet on high-reward jar, 0: bet on low-reward jar
			subdata = subindices(blockindices == blockidx & beadsdif == dif & rewindices == rewidx);

			% average
			populmean(rewidx + 1, find(beadsdiflevels == dif)) = mean(betdata);

			% bootstrap SEM. resample participants (only those that have non-zero trials in the specific beads difference and reward condition)
			nonzerosub = unique(subdata);
			bootmean = zeros(bootiternum, 1);
			parfor bootiter = 1:bootiternum
				bootsub = randi(length(nonzerosub), length(nonzerosub), 1);
				bootdata = [];
				for sub = 1:length(nonzerosub)
					bootdata = [bootdata; betdata(subdata == nonzerosub(bootsub(sub)))];
				end
				bootmean(bootiter) = mean(bootdata);
			end
			populse(rewidx + 1, find(beadsdiflevels == dif)) = std(bootmean);
		end
	end

	%% visualize observed behavior and model fit
	figure(1); subplot(2, 1, blockidx); hold on;
	plot(pred_beadsdif, choice_phat/2+0.5, '-', 'Color', colorblock(blockidx, :), 'LineWidth', 2);	%% model fit

	offset = 0.05;
	plot([beadsdiflevels+offset; beadsdiflevels+offset], [populmean(1, :) + populse(1, :); populmean(1, :) - populse(1, :)], '-', 'Color', colorblock(blockidx, :), 'LineWidth', 1);
	plot([beadsdiflevels-offset; beadsdiflevels-offset], [populmean(2, :) + populse(2, :); populmean(2, :) - populse(2, :)], '-', 'Color', colorblock(blockidx, :), 'LineWidth', 1);
	plot(beadsdiflevels+offset, populmean(1, :), 'd', 'MarkerEdgeColor', colorblock(blockidx, :), 'MarkerFaceColor', 'w', 'MarkerSize', 8, 'LineWidth', 1);
	plot(beadsdiflevels-offset, populmean(2, :), 'o', 'MarkerEdgeColor', colorblock(blockidx, :), 'MarkerFaceColor', colorblock(blockidx, :), 'MarkerSize', 8, 'LineWidth', 1);

	xlim([-12, 12]); ylim([0, 1]); set(gca, 'XTick', -12:6:12, 'YTick', 0:0.5:1);
end

saveas(1, 'betchoice_modelfit.pdf');
close(1);

end
